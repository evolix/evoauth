package Evoauth::Admin;

use 5.006001;
use strict;
use warnings;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

our %EXPORT_TAGS = ( 'all' => [ qw( 
	
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

our $VERSION = '0.4';


1;
__END__

=head1 NAME

Evoauth::Admin - Admin

=head1 SYNOPSIS

  use Evoauth::Admin;

=head1 DESCRIPTION

Fonctions d'administration d'Evoauth.

=head2 EXPORT

...

=head1 SEE ALSO

...

=head1 AUTHOR

Evolix, E<lt>info@evolix.fr<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2005 Evolix

Licence GPL.

=cut

