package Evoauth::Functions;

use strict;
use warnings;
use Config::Tiny;
use DBI;
use MIME::Lite;

# Renvoit la date courrante
sub Date() {
	my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = 
		localtime(time);

	$year += 1900;
	$mon++;

	my $temps = "$mday/$mon/$year - $hour:$min:$sec";
	return $temps;
}

# Envoit un mail
sub Mail() {
	my ($event, $ip) = @_;
	my $temps = Date();

	# paramètres de connexion
	my $Config = Config::Tiny->read( '/etc/evoauth/evoauth.conf' );
	my $db = $Config->{bdd}->{db};
	my $username = $Config->{bdd}->{username};
	my $userpass = $Config->{bdd}->{userpass};

	# connexion
	my $dbh = DBI->connect( $db, $username, $userpass ) ||
		&Log("La connexion a échoué : $DBI::errstr");

	# récupération du login correspondant à l'ip
	my $sql = "SELECT login FROM users where ip = '".$ip."'";

	my $sth = $dbh->prepare($sql);
	$sth->execute();

	my $login;
	$sth->bind_columns(undef, \$login) && $sth->fetch();

	my $msg;
 
	if ($login eq "") {
		$msg = new MIME::Lite
			From    => 'evoauth@shaktiware.fr',
			To      => 'auth@shaktiware.fr',
			CC      => '',
			Subject => "$event annulée",
			Type    => 'TEXT',
			Data    => "L'ip $ip de l'utilisateur a changé, suppression...";
	}
 
	else {
		$msg = new MIME::Lite
			From    => 'evoauth@shaktiware.fr',
			To      => 'auth@shaktiware.fr',
			CC      => '',
			Subject => "$event de $login",
			Type    => 'TEXT',
			Data    => "$temps : $event de $login ($ip)";
	}

	$msg -> send && &Log("Un mail a été envoyé.");
}

# Ecrit dans le journal
sub Log() {
	my $file = "/var/log/evoauth.log";
	my $message = shift;
	my $temps = &Date;

	open(LOG, ">> $file") or 
		die "L'ouverture du journal evoauth.log a échoué: $!.\n";

	print LOG "$temps $message\n";

	close(LOG);
}

1;
__END__

=head1 NAME

Evoauth::Functions - Fonctions

=head1 SYNOPSIS

  use Evoauth::Functions;

=head1 DESCRIPTION

Fonctions d'administration d'Evoauth.

=head2 EXPORT

...

=head1 SEE ALSO

...

=head1 AUTHOR

Evolix, E<lt>info@evolix.fr<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2005 Evolix

Licence GPL.

=cut
