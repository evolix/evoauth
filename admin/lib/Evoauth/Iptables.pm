package Evoauth::Iptables;

use strict;
use warnings;
use Config::Tiny;
use Evoauth::Functions;
use DBI;

my $Config = Config::Tiny->read( '/etc/evoauth/evoauth.conf' );

# Paramètres de configuration
my $activation = $Config->{control}->{enable};
my $timetorem = $Config->{control}->{timetorem};

# Paramètres Base de données
my $db = $Config->{bdd}->{db};
my $username = $Config->{bdd}->{username};
my $userpass = $Config->{bdd}->{userpass};

# Règles firewall
open(RULES, "/etc/evoauth/evoauth.rules") ||
	&Log("L'ouverture du fichier de règles a échoué.");
our @rules = <RULES>;
close(RULES);

sub Alter() {
	my $action = shift;
	my $ip = shift;

	# mode ajout
	if ($action == 1) {
		system("/sbin/iptables -I EVOAUTH -s $ip -j ACCEPT 2>/dev/null");
		&Evoauth::Functions::Log("[E] Connexion de $ip.") &&
			&Evoauth::Functions::Mail("[E] Connexion", $ip);
	}

	# mode vérification
	elsif ($action == 2) {
		&check_iptables;
		&check_timestamp;
	}

	# mode suppression
	else {

		my $dbh = DBI->connect( $db, $username, $userpass ) ||
			&Evoauth::Functions::Log("[W] La connexion a échoué : $DBI::errstr");

		my $sql = qq{ UPDATE users set statut = 0 where ip = '$ip' };
		my $sth = $dbh->prepare($sql);

		system("/sbin/iptables -D EVOAUTH -s $ip -j ACCEPT 2>/dev/null");

		$sth->execute();
		$sth->finish();

		&Evoauth::Functions::Log("[E] Déconnexion de $ip.") &&
			&Evoauth::Functions::Mail("[E] Déconnexion", $ip);
	}
}


sub check_iptables() {
	my ($ip, @ips);

	&Evoauth::Functions::Log("[C] Vérification des règles Iptables.");

	# obtention de la liste des ips
	system("/sbin/iptables -L EVOAUTH -n | grep ACCEPT | awk '{ print \$4 }' > /tmp/ips.txt");

	# on ouvre le fichier des ips
	open(IPS, "/tmp/ips.txt") || &Log("[W] L'ouverture des IPs a échoué.");
	@ips = <IPS>;
	close(IPS);

	foreach $ip (@ips) {
		chomp $ip;

		my $dbh = DBI->connect( $db, $username, $userpass ) ||
			&Evoauth::Functions::Log("[W] La connexion a échoué : $DBI::errstr");

		my $sql = "SELECT statut FROM users where ip = '".$ip."'";
		my $sth = $dbh->prepare( $sql );
		$sth->execute();
        
		my $statut;
		$sth->bind_columns(undef, \$statut);
		$sth->fetch();

		# si entrée iptables présente mais statut non connecté, on supprime
		if (defined($statut)) {
			if ($statut != 1) {
				&Evoauth::Functions::Log("[A] Suppression de $ip.") &&
					&Evoauth::Functions::Mail("[A] Suppression", $ip);
				&Alter(3, $ip);
			}
		}

		else {
			&Evoauth::Functions::Log("[A] Suppression de $ip.") &&
				&Evoauth::Functions::Mail("[A] Suppression", $ip);
			&Alter(3, $ip);
		}
	}
}

sub check_timestamp() {
	&Evoauth::Functions::Log("[C] Vérification des états de connexion.");

	# on travaille sur tous les utilisateurs présents
	my $dbh = DBI->connect( $db, $username, $userpass ) ||
		&Evoauth::Functions::Log("[W] La connexion a échoué : $DBI::errstr");

	my $sql = "SELECT * FROM users";
	my $sth = $dbh->prepare($sql);
	$sth->execute();

	my($id, $login, $pass, $groupe, $utype, $credit, $ip, $statut, 
	$actif, $firstcon, $lastupdate, $kick);

	$sth->bind_columns(undef, \$id, \$login, \$pass, \$groupe,
	\$utype, \$credit, \$ip, \$statut, \$actif, \$firstcon,
	\$lastupdate, \$kick);

	&Evoauth::Functions::Log("[A] Vérification de la base.");

	my ($newtime, $oldtime, $cpt);

	while ($sth->fetch() && $sth != 0) {
		if ($statut == 1) {
			$newtime = time();
			$oldtime = $lastupdate;

			# si le dernier update est trop ancien, on supprime
			my $timestamp = $newtime - $oldtime;
			if ($timestamp > $timetorem) {
				# dernière connexion est < 1 min -> suppresion
				&Alter(3, $ip);
				&Evoauth::Functions::Log("[A] Suppression de $ip.") &&
					&Evoauth::Functions::Mail("[A] Suppression", $ip);
			}

			# sinon conservation
			else {
				&Evoauth::Functions::Log("[A] Conservation de $ip.");
			}
		}
	}

	$sth->finish();
}

sub Control() {
	my $action = shift;

	# initialisation d'Evoauth
	if ($action == 1) {
		foreach (@rules) {
			next if /^#/;
			chomp;

			# on supprimer les commentaires
			$_ =~ s/#.*//;

			# on split la liste des paramètres
			my @tmp1 = split (/\t+|\s+/);

			system("/sbin/iptables -t nat -A PREROUTING -p $tmp1[3] -i ppp0 --dport $tmp1[1] -j DNAT --to $tmp1[0]:$tmp1[2] 2>/dev/null");
		}

		&Evoauth::Functions::Log("[C] 1- Règles de PREROUTING chargées");

		system("/sbin/iptables -N EVOAUTH 2>/dev/null");
		system("/sbin/iptables -A EVOAUTH -j DROP 2>/dev/null");

		&Evoauth::Functions::Log("[C] 2 - Tables crées");

		# chargement des règles
		foreach (@rules) {
			next if /^#/;
			chomp;

			# on supprimer les commentaires
			$_ =~ s/#.*//;

			# on split la liste des paramètres
			my @tmp2 = split (/\t+|\s+/);

			system("/sbin/iptables -A FORWARD -p $tmp2[3] -i ppp0 -o eth0 --dport $tmp2[1] -j EVOAUTH 2>/dev/null");
		}

		&Evoauth::Functions::Log("[C] 3 - Règles chargées");

		&Evoauth::Functions::Log("[C] Evoauth vient de démarrer.");
	}

	# arret
	elsif ($action == 2) {
		system("/sbin/iptables -F EVOAUTH 2>/dev/null");
		&Evoauth::Functions::Log("[C] 1 - Flush de la table EVOAUTH");

		foreach (@rules) {
			next if /^#/;
			chomp;

			# on supprimer les commentaires
			$_ =~ s/#.*//;

			# on split la liste des paramètres
			my @tmp3 = split (/\t+|\s+/);

			system("/sbin/iptables -D FORWARD -p $tmp3[3] -i ppp0 -o eth0 --dport $tmp3[1] -j EVOAUTH 2>/dev/null");
			system("/sbin/iptables -t nat -D PREROUTING -p $tmp3[3] -i ppp0 --dport $tmp3[1] -j DNAT --to $tmp3[0]:$tmp3[2] 2>/dev/null");
		}

		&Evoauth::Functions::Log("[C] 2 - Annulation FORWARD + PREROUTING");

		system("/sbin/iptables -X EVOAUTH 2>/dev/null");
		&Evoauth::Functions::Log("[C] 3 - Suppression de la table EVOAUTH");

		&Evoauth::Functions::Log("[C] Evoauth vient de s'arreter.");
	}

	# restart
	else {
		&Control(2);
		&Control(1);
		&Evoauth::Functions::Log("[C] Evoauth vient de redémarrer.");
	}
}

1;
__END__

=head1 NAME

Evoauth::Iptables - Firewall

=head1 SYNOPSIS

use Evoauth::Iptables;

=head1 DESCRIPTION

Fonctions d'administration d'Evoauth.

=head2 EXPORT

...

=head1 SEE ALSO

...

=head1 AUTHOR

Evolix, E<lt>info@evolix.fr<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2005 Evolix

Licence GPL.

=cut
