#!/usr/bin/perl

package main;

use strict;
use warnings;
use Getopt::Std;
use Config::Tiny;
use Fcntl;

use Evoauth::Admin;
use Evoauth::Functions;
use Evoauth::Iptables;

$SIG{INT} = $SIG{TERM} = $SIG{KILL} = "";

sysopen(LOCK, "/tmp/evoauth.lock", O_WRONLY|O_EXCL|O_CREAT)
	or die "Le verouillage a échoué.";

# choix des options
my %options=();
getopts("icsd:a:",\%options);

if (defined $options{a})
	{ &Evoauth::Iptables::Alter(1, $options{a}); }
elsif (defined $options{d})
	{ &Evoauth::Iptables::Alter(3, $options{d});}
elsif (defined $options{c})
	{ &Evoauth::Iptables::Alter(2); }
elsif (defined $options{i})
	{ &Evoauth::Iptables::Control(1); }
elsif (defined $options{s})
	{ &Evoauth::Iptables::Control(2); }
else
	{ &Usage; }

sub Usage() {
	print "\nusage :\n";
	print "-i    : initialisation du logiciel\n";
	print "-s    : arrêt du logiciel\n";
	print "-a ip : ajout d'une ip a la base\n";
	print "-d ip : suppression d'une ip a la base\n";
	print "-c    : vérification des bases de connexion\n";
	print "-h    : aide\n";

	exit;
}

END {
	unlink "/tmp/evoauth.lock";
}
