<?
require_once "includes/config.php";
require "includes/database.php";

session_name("EVOAUTH_PHPSESSION");
session_start();

delrules();

// suppression des règles concernées
function delrules()
{
	// suppression des règles de firewall
	system("/usr/bin/sudo /usr/local/bin/evoauth -d ".$_SESSION['ip']." > /dev/null");
	update_statut($_SESSION['login'], "0");
}

session_unset("EVOAUTH_PHPSESSION");
session_destroy();

?>


<html>
<head></head>
<body>
<table align="center">
<tr>
  <td><b>À bientôt.</b></td>
  <td><a href="index.php">Retour à l'index</a></td>
</tr>
</table>
</body>
</html>
