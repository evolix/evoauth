<?
require_once "config.php";

// connexion à la base
function connexion()
{
        global $db_host;
        global $db_login;
        global $db_pass;
        global $db_name;

	$connexion = mysql_pconnect($db_host, $db_login, $db_pass) or die("Problème: ".mysql_error());
	mysql_select_db($db_name, $connexion) or die("Désolé accès à la base impossible: ".mysql_error());

	return $connexion;
}

# sélection du pass en fonction du login
function getpass($login)
{
	$connexion = connexion();

	$query = 'select pass from users where login = \''.$login.'\'';

	$resultat = mysql_query($query) or die("Erreur getpass: ".mysql_error());
	$valeur = mysql_fetch_object($resultat);
	mysql_close ($connexion);

	return $valeur->pass;
}


# sélection du groupe en fonction du login
function getgroup($login)
{
	$connexion = connexion();

	$query= 'select groupe from users where login = \''.$login.'\'';

	$resultat = mysql_query($query) or die("Erreur getgroup: ".mysql_error());
	$valeur = mysql_fetch_object($resultat);
	mysql_close ($connexion);

	return $valeur->groupe;
}


# renvoie le crédit restant de l'utilisateur passé en paramètre
function getcredit($login)
{
	$connexion = connexion();

	$query= 'select credit from users where login = \''.$login.'\'';

	$resultat = mysql_query($query) or die("Erreur getcredit: ".mysql_error());
	$valeur = mysql_fetch_object($resultat);
	mysql_close ($connexion);

	return $valeur->credit;
}


# indique si l'utilisateur est activé ou désactivé
function getactif($login)
{
	$connexion = connexion();

	$query= 'select actif from users where login = \''.$login.'\'';

	$resultat = mysql_query($query) or die("Erreur getactif: ".mysql_error());
	$valeur = mysql_fetch_object($resultat);
	mysql_close ($connexion);

	return $valeur->actif;
}


# renvoit le type de l'utilisateur passé en paramètre
function getutype($login)
{
	$connexion = connexion();

	$query= 'select utype from users where login = \''.$login.'\'';

	$resultat = mysql_query($query) or die("Erreur getutype: ".mysql_error());
	$valeur = mysql_fetch_object($resultat);
	mysql_close ($connexion);

	return $valeur->utype;
}


# renvoit le statut de l'utilisateur passé en paramètre
function getstatut($login)
{
	$connexion = connexion();

	$query= 'select statut from users where login = \''.$login.'\'';

	$resultat = mysql_query($query) or die("Erreur getstatut: ".mysql_error());
	$valeur = mysql_fetch_object($resultat);
	mysql_close ($connexion);

	return $valeur->statut;
}


# mise à jour du crédit de l'utilisateur
function setcredit($login, $credit)
{
	$connexion = connexion();

	$query = 'update users set credit = \''.$credit.'\' where
	login = \''.$login.'\'';

	$resultat = mysql_query($query) or die("Erreur setcredit: ".mysql_error());
	mysql_close ($connexion);

	return 1;
}


# mise à jour du mot de passe
function update_pass($newpass)
{
	$connexion = connexion();

	$query = 'update users set pass = \''.$newpass.'\' where
	login = \''.$_SESSION['login'].'\'';

	$resultat = mysql_query($query) or die("Erreur update_pass: ".mysql_error());
	mysql_close ($connexion);

	return 1;
}


# mise à jour de l'adresse ip du client
function update_ip($log, $ip)
{
	$connexion = connexion();

	$query = 'update users set ip = \''.$ip.'\' where
	login = \''.$log.'\'';

	$resultat = mysql_query($query) or die("Erreur update_ip: ".mysql_error());
	mysql_close ($connexion);

	return 1;
}


# nettoyage des ips de la base a la connexion
function clean_ip($log, $ip)
{
        $connexion = connexion();

	$blank = "0.0.0.0";
        $query = 'update users set ip = \''.$blank.'\' where ip = \''.$ip.'\'
	and login != \''.$log.'\'';

        $resultat = mysql_query($query) or die("Erreur clean_ip: ".mysql_error());
        mysql_close ($connexion);

        return 1;
}


# mise à jour du statut du client
function update_statut($log, $statut)
{
	$connexion = connexion();

	$query = 'update users set statut = \''.$statut.'\' where
	login = \''.$log.'\'';

	$resultat = mysql_query($query) or die("Erreur update_statut: ".mysql_error());
	mysql_close ($connexion);

	return 1;
}


# mise à jour de lastupdate
function update_lastupdate($log, $lastupdate)
{
	$connexion = connexion();

	$query = 'update users set lastupdate = \''.$lastupdate.'\' where
	login = \''.$log.'\'';

	$resultat = mysql_query($query) or die("Erreur update_lastupdate: ".mysql_error());
	mysql_close ($connexion);

	return 1;
}


# recherche d'un utilisateur du même nom
function seek_for_user($newlogin)
{
	$connexion = connexion();

	$query = 'select login from users where login = \''.$newlogin.'\'';

	$resultat = mysql_query($query);
	$nombre = mysql_num_rows($resultat);
	mysql_close ($connexion);

	if ($nombre) { return 1; }
	else { return 0; }
}


# mise à jour du mot de passe
function add_user($newlogin, $pass, $newgroup, $utype, $credit)
{
	$connexion = connexion();

	$query = 'insert into users (login, pass, groupe, utype, credit, actif) VALUES(\''.$newlogin.'\',
	\''.$pass. '\', \''.$newgroup.'\', \''.$utype.'\', \''.$credit.'\', "1")';

	$resultat = mysql_query($query) or die("Erreur add_user: ".mysql_error());

	return 1;
}


# mise à jour de la variable kick
function setkick($ip, $value)
{
	$connexion = connexion();

	$query = 'update users set kick = \''.$value.'\' where
	ip = \''.$ip.'\'';

	$resultat = mysql_query($query) or die("Erreur setkick: ".mysql_error());
	mysql_close ($connexion);

	return 1;
}


# renvoit la valeur de kick de l'utilisateur passé en paramètre
function getkick($login)
{
	$connexion = connexion();

	$query= 'select kick from users where login = \''.$login.'\'';

	$resultat = mysql_query($query) or die("Erreur getkick: ".mysql_error());
	$valeur = mysql_fetch_object($resultat);
	mysql_close ($connexion);

	return $valeur->kick;
}
?>
