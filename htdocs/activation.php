<?

require_once "includes/config.php";
require_once "includes/database.php";
require_once "includes/fonctions.php";

session_name("EVOAUTH_PHPSESSION");
session_start();

$statut = getstatut($_SESSION['login']);

// Déconnecté -> Connecté
if ($statut == "0") {
	echo '<html><head>';
	echo '<script>';
	echo 'window.open(\'popup_debut.php\', \'connexion\', \'height=147, 
						width=300, toolbar=no, menubar=no,
						scrollbars=no, resizable=no, status=no\')';
	echo '</script>';
	echo '</head><body>';
	echo '<p>Vous êtes connecté.';
	echo '<p>Pour revenir à l\'accueil tout en restant connecté, cliquez
						<a href="membre.php">ici</a>.';
	echo '</body></html>';

	loadrules();
}

// Connecté mais plus de popup
elseif ($statut == "1" && $_GET['pop'] == 1) {
	echo '<html><head>';
	echo '<script>';
	echo 'window.open(\'popup_debut.php\', \'connexion\', \'height=147, 
						width=300, toolbar=no, menubar=no,
						scrollbars=no, resizable=no, status=no\')';
	echo '</script>';
	echo '</head><body>';
	echo '<p>Vous avez réactivé votre connexion.';
	echo '<p>Pour revenir à l\'accueil tout en restant connecté, cliquez
						<a href="membre.php" target="_self">ici</a>.';
	echo '<p>Pour ouvrir à nouveau votre popup de connexion, cliquez
						<a href="activation.php?pop=1">ici</a>.';
	echo '<p>Pour vous déconnecter, cliquez <a href="fin.php">ici</a>.';
	echo '</body></html>';
}

// Connecté -> Connecté
elseif ($statut == "1") {
	echo '<html><head>';
	echo '</head><body>';
	echo '<p>Vous êtes déjà connecté.';
	echo '<p>Pour revenir à l\'accueil tout en restant connecté, cliquez
						<a href="membre.php" target="_self">ici</a>.';
	echo '<p>Pour ouvrir à nouveau votre popup de connexion, cliquez
						<a href="activation.php?pop=1">ici</a>.';
	echo '<p>Pour vous déconnecter, cliquez <a href="fin.php">ici</a>.';
	echo '</body></html>';
}

else {
	kick("Accès non autorisé");
}

// chargement des règles concernées
function loadrules()
{
	// activation des règles de firewall
	system("/usr/bin/sudo /usr/local/bin/evoauth -a ".$_SESSION['ip']);

	// passage au statut authentifié
	update_statut($_SESSION['login'], "1");
}
?>
