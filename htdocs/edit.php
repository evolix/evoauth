<?php

require_once "includes/config.php";
require "includes/database.php";
require "includes/fonctions.php";

session_name("EVOAUTH_PHPSESSION");
session_start ();

$mode = $_GET['mode'];
$flag = $_GET['flag'];

// accès OK
if (isset($_SESSION['login']))
{
	if (!empty($_POST))
	{
		if (isset($_POST['ajout']))
		{
			$mode = "add";
			$flag = 0;
		}

		elseif (isset($_POST['suppression']))
		{
			$mode = "suppression";
			$flag = 1;
		}

		elseif (isset($_POST['activation']))
		{
			$mode = "activation";
			$flag = 1;
		}

		elseif (isset($_POST['desactivation']))
		{
			$mode = "desactivation";
			$flag = 1;
		}

		elseif (isset($_POST['kick']))
		{
			$mode = "kick";
			$flag = 1;
		}

		else
		{
			echo '';
		}
	}




	/* mode changement du mot de passe utilisateur */

	if ($mode == "pass" && $flag == 1)
	{
		if ($_POST['pass1'] == $_POST['pass2'])
		{
			// mise à jour du pass
			$newpass = md5($_POST['pass1']);
			update_pass($newpass);

			// changement effectué, on redirige vers la page d'accueil
			header("location: membre.php");
		}

		else
		{
			// le changement a échoué
			header("location: membre.php");
		}
	} /* fin mode changement du mot de passe utilisateur */



	/* mode ajout d'un utilisateur */

	elseif ($mode == "add" && $flag == 1 && $_SESSION['login'] == "admin")
	{
		// l'utilisateur a créer existe déjà
		$resultat = seek_for_user($_POST['newlogin']);

		if ($resultat == 1) 
		{
			header("location: membre.php"");
		}

		// les 2 mots de passe saisis sont différents
		elseif ($_POST['pass1'] != $_POST['pass2'])
		{
			header("location: membre.php"");
		}

		// le nouvel utilisateur a un crédit nul
		elseif ($_POST['utype'] == 1 && $_POST['credit'] == 0)
		{
			header("location: membre.php"");
		}
			
		// tout est OK, on peut ajouter l'utilisateur
		else
		{
			// création des paramètres
			$newlogin = $_POST['newlogin'];
			$newpass = md5($_POST['pass1']);
			(defined($_POST['newgroup'])) ? $newgroup=$_POST['newgroup'] : $newgroup="general";

			if ($_POST['utype'] == 0)
				$newutype="0";
			elseif ($_POST['utype'] == 1)
				$newutype="1";
			else
				$newutype="0";
			
			$newcredit = $_POST['credit'];
			$resultat = add_user($newlogin, $newpass, $newgroup, $newutype, $newcredit);

			// ajout réussi
			if ($resultat)
			{
				header("location: membre.php"");
			}

			// ajout échoué
			else
			{
				header("location: membre.php"");
			}
		}
	} /* fin mode ajout d'un utilisateur */


 
	/* mode suppression */

	elseif ($mode == "suppression" && $flag == 1 && $_SESSION['login'] == "admin")
	{
		// correspondance trouvee
		if (!empty($_POST['coche']))
		{
			foreach ($_POST['coche'] as $coche)
			{
				$connexion = connexion();

		                $requete= "delete from users where id='$coche'";
		                $resultat =mysql_query($requete, $connexion);

				// suppression correctement effectuée
				if ($resultat == 1)
				{
					header("location: edit.php?mode=listing");
				}

				// suppression échouée
				else
				{
					header("location: membre.php");
				}
				
     		}
		}

		// aucun utilisateur a supprimer
		else
		{
			header("location: edit.php?mode=listing");
		}
	} /* mode suppression */



	/* mode activation */

	elseif ($mode == "activation" && $flag == 1 && $_SESSION['login'] == "admin")
	{
		// utilisateur à activer
		if (!empty($_POST['coche']))
		{
			foreach ($_POST['coche'] as $coche)
			{
				$connexion = connexion();

		                $requete= "update users set actif = 1 where id='$coche'";
		                $resultat =mysql_query($requete, $connexion);

				// activation effectuée
				if ($resultat == 1)
				{
					header("location: edit.php?mode=listing");
				}

				// activation échouée
				else
				{
					header("location: membre.php");
				}
   			}
		}

		// aucun utilisateur à activer
		else
		{
			header("location: edit.php?mode=listing");
		}
	} /* fin mode activation */



	/* mode desactivation */

	elseif ($mode == "desactivation" && $flag == 1 && $_SESSION['login'] == "admin")
	{
		if (!empty($_POST['coche']))
		{
			foreach ($_POST['coche'] as $coche)
			{
				$connexion = connexion();

		                $requete= "update users set actif = 0 where id='$coche'";
		                $resultat =mysql_query($requete, $connexion);

				// désactivation effectuée
				if ($resultat == 1)
				{
					header("location: edit.php?mode=listing");
				}

				// ehec de la désactivation
				else
				{
					header("location: membre.php");
				}
   			}
		}

		// aucun utilisateur à désactiver
		else
		{
			header("location: edit.php?mode=listing");
		}
	} /* fin mode desactivation */



	/* mode kick */

	elseif ($mode == "kick" && $flag == 1 && $_SESSION['login'] == "admin")
	{
		if (!empty($_POST['coche']))
		{
			foreach ($_POST['coche'] as $coche)
			{
				$connexion = connexion();

		                $requete= "select ip from users where id='$coche' and statut='1'";
				$resultat = mysql_fetch_row(mysql_query ($requete, $connexion));

				// l'utilisateur n'est pas connecte
                if ($resultat == 0)
				{
					header("location: edit.php?mode=listing");
				}

				// kick
				else
				{
					$ip = current($resultat);

					// désactivation de l'utilisateur dans le firewall
					system("/usr/bin/sudo /usr/local/bin/evoauth -d $ip");

					// variable nécessaire pour interdir le prochain refresh
					setkick($ip, "1");

					header("location: membre.php");
				}
   			}
		}

		// aucun utilisateur à kicker
		else
		{
			header("location: edit.php?mode=listing");
		}
	} /* fin mode kick */


        /* mode accueil */

        elseif ($mode == accueil)
	{
?>
	        <html>
                <head>
                <link rel="StyleSheet" href="style.css" type="text/css">
                <title>Intranet <?=$title;?></title>
                </head>
                <body>
                <p align="center"><img src="images/<?=$pic;?>"></p>
                <br>
<?
		// relecture de la page et enregistrement
		if ($flag == 1) {
	                if (!$file = fopen("accueil.txt", "w")) {
        	                echo "Echec de l'ouverture du texte d'accueil (accueil.txt)";
			}

			$text = $_POST["acc"];
			fputs($file, $text);
			fclose($file);
		}
			

                // on ouvre accueil.txt
                if (!$file = fopen("accueil.txt", "r")) {
                        echo "Echec de l'ouverture du texte d'accueil (accueil.txt)";
                }

                else {
                        // on parcourt accueil.txt
                        while (!feof($file))
                        {
                                $accueil .= fgets($file, 255);
                        }
                }
?>
		<p align="center">Modifier le texte d'accueil</p>
		<form action="edit.php?mode=accueil&flag=1" method="post">
		<table align="center">
		<tr>
		<td>Votre texte :</td>
		<td>
<textarea name="acc" rows="10" cols="80" wrap="PHYSICAL">
<?=$accueil?>
</textarea>
		</td>
		</tr>
		<tr>
		<td colspan="2" align="center"><input type="submit" name="submit" value="Mise a jour"></td>
		</tr>
		</table>
		</form>
		<p align="center"><br><br>
		<a href="membre.php">Accueil</a>
		<a href="index.php">Déconnexion</a>
		</body>
		</html>
<?

                fclose($file);
	} /* fin mode accueil */


	/* mode normal de visualisation */

	else
	{

?>
		<html>
		<head>
		<link rel="StyleSheet" href="style.css" type="text/css">
		<title>Intranet <?=$title;?></title>
		</head>
		<body>
		<p align="center"><img src="images/<?=$pic;?>"></p>
		<br>
<?
		if ($mode == "pass" && $_SESSION['login'] != "admin")
		{
?>
			<p align="center">Changer votre mot de passe</p>
			<form action="edit.php?mode=pass&flag=1" method="post">
			<table align="center">
			<tr>
			<td>Login :</td>
			<td><?=$_SESSION['login']?></td>
			</tr>
			<tr>
			<td>Nouveau mot de passe :</td>
			<td><input type="password" name="pass1" style="background:red;color:yellow"></td>
			</tr>
			<tr>
			<td>Confirmation :</td>
			<td><input type="password" name="pass2" style="background:red;color:yellow"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr>
			<td colspan="2" align="center"><input type="submit" name="submit" value="Mise à jour"></td>
			</tr>
<?
		}

		// ajout d'utilisateur si la personnes est admin
		elseif ($mode == "add" && $_SESSION['login'] == "admin")
		{
?>
			<p align="center">Ajouter un utilisateur</p>
			<form method="post" action="edit.php?mode=add&flag=1">
			<table align="center">
			<tr>
			<td>Login :</td>
			<td><input type="text" name="newlogin"></td>
			</tr>
			<tr>
			<td>Groupe :</td>
			<td><input type="text" name="newgroup" value="general"></td>
			</tr>
			<tr>
			<td>Mot de passe :</td>
			<td><input type="password" name="pass1" style="background:red;color:yellow"></td>
			</tr>
			<tr>
			<td>Confirmation :</td>
			<td><input type="password" name="pass2" style="background:red;color:yellow"></td>
			</tr>
<?
			if ($prepaid == 1)
			{
?>
				<tr>
				<td>Type :</td>
				<td>
				Permanent <input type="radio" name="utype" value="0">
				<input type="radio" name="utype" value="1"> Crédité
				</td>
				</tr>
				<tr>
				<td>Crédit (utile si utilisateur crédité) :</td>
				<td><input type="text" name="credit"></td>
				</tr>
<?
			}
?>
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr>
			<td colspan="2" align="center"><input type="submit" name="submit" value="Ajout"></td>
<?
		}

		// listing dans une optique de suppression
		elseif ($mode == "listing" && $_SESSION['login'] == "admin")
		{
			$connexion = connexion();
			$resultat= mysql_query ("select * from users order by id", $connexion);
?>
			<form method="post" action="edit.php?mode=listing&flag=1">
			<table border="1" bordercolor="black" align="center" width="95%">
			<tr>
			<td align="center">&nbsp;</td>
			<td align="center"><b>N°</b></td>
			<td align="center"><b>Login</b></td>
			<td align="center"><b>Groupe</b></td>
			<td align="center"><b>Statut</b></td>
			<td align="center"><b>Dernière connexion</b></td>
			<td align="center"><b>Compte</b></td>
<?
			if ($prepaid == 1)
			{
?>
				<td align="center"><b>Type</b></td>
				<td align="center"><b>Crédit restant</b></td>
				</tr>
<?
			}

			while ($byblos = mysql_fetch_object ($resultat))
			{
				// génération d'un horodatage agréable
				if ($byblos->lastupdate != 0)
				{
					$horodatage = date("d/m/Y, H:i:s", $byblos->lastupdate);
				}

				else { $horodatage = "Aucune"; }
?>
				<tr>
				<td><input type="checkbox" name="coche[]" value=<?=$byblos->id?>></td>
				<td><?=$byblos->id?></td>
				<td><?=$byblos->login?></td>
				<td><?=$byblos->groupe?></td>
				<td><b><font color="red"><?=($byblos->statut==1)?"Connecté":"Non connecté"?></font></b></td>
				<td><?=$horodatage?></td>
				<td><b><?=($byblos->actif==1)?"Activé":"Désactivé"?></b></td>
<?
				if ($prepaid == 1)
				{
?>
					<td><?=($byblos->utype==0)?"Permanent":"Crédité"?></td>
<?
					if ($byblos->utype == 0)
					{
?>
						<td bgcolor="lightgrey"></td>
<?
					}

					else
					{
?>
						<td><?=$byblos->credit?> minutes</td>
<?
					}
?>
					</tr>
<?
				}
			}
?>
			<tr>
                       	<td colspan="<?=($prepaid==1)?11:8?>" align="center">
			<input type="submit" name="ajout" value="ajout">
			<input type="submit" name="suppression" value="suppression">
			<input type="submit" name="activation" value="activation">
			<input type="submit" name="desactivation" value="desactivation">
			<input type="submit" name="kick" value="kick">
			</td>
                       	</tr>
<?
		}
?>
		</form>
		</table>
		<p align="center"><br><br>
		<a href="membre.php">Accueil</a>
		<a href="index.php">Déconnexion</a>
		</body>
		</html>
<?
	} /* fin mode normal de visualisation */
}

// accès refusé //

else
{
	kick("Accès refusé.");
}	
?>
