<?
require_once "includes/config.php";
require "includes/database.php";

session_name("EVOAUTH_PHPSESSION");
session_start();

delrules();

// suppression des règles concernées
function delrules()
{
	// suppression des règles de firewall
	system("/usr/bin/sudo /usr/local/bin/evoauth -d ".$_SESSION['ip']."> /dev/null");
	update_statut($_SESSION['login'], "0");
}

session_unset("EVOAUTH_PHPSESSION");
session_destroy();

?>

<html>
<body text="black" alink="black" vlink="black">
<table align="center">
<tr>
<td><b>A bientôt.</b></td>
</tr><tr>
<td align="center"></td>
</tr><tr>
<td align="center"><a href="javascript: window.close()"><img src="images/fin.png"></a></td>
</tr>
</tr><tr>
<td align="center"><font size="1" color="black">Fermer</font></td>
</tr>
</table>
</body>
</html>
