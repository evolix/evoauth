<?
require_once "includes/config.php";
require "includes/database.php";
require "includes/fonctions.php";

session_name("EVOAUTH_PHPSESSION");
session_start();

// on vérifie que la session est active et que l'ip est correcte
$isip = getenv("REMOTE_ADDR");
$kick = getkick($_SESSION['login']);

if ($_SESSION['ip'] != $isip) {
	exit;
}

elseif ($kick == "1") {
	setkick($_SESSION['login'], "0");
	header("location:popup_fin.php");
}

else {
	// données à insérer
   	$temps = time();
	$ligne = $temps." ".$_SESSION['ip'];

	if ($prepaid == 1 && getutype($_SESSION['login']) == "1")
	{
		// on récupère le crédit restant de l'utilisateur
		$credit = getcredit($_SESSION['login']);

		$credit--;	

		// actualisation de la base de données
		setcredit($_SESSION['login'], $credit);
	}

	update_statut($_SESSION['login'], "1");

	$newt = time();
	update_lastupdate($_SESSION['login'], $newt);

	// crédit épuisé
	if (getutype($_SESSION['login']) == "1" && $prepaid == 1 && $credit <= 0)
	{
		header("location: popup_fin.php");
	}

	// il reste du crédit
	else
	{
?>

		<html>
		<head>
		<meta http-equiv="Refresh" content="60;popup_debut.php">
		<script language = "JavaScript">
		function fin() {
			window.open('popup_debut.php', 'connexion', 'height=<?=$height?>, width=<?=$width?>, toolbar=no, menubar=no, scrollbars=no, resizable=no, status=no')
		}
		function sortie() {
			if(confirm('Etes vous sur de vouloir quitter ?'))
				document.location.href='popup_fin.php'
		}
		</script>
		</head>
		<body text="black" alink="black" vlink="black" onUnload="fin()">
		<table align="center">
		<tr>
<?
		if (getutype($_SESSION['login']) == "1" && $prepaid == 1 && $credit == 1)
		{
?>
			<td colspan="2" align="center">
			<b><font color="red">Attention, dernière minute de connexion...</font></b>
			</td>
<?
		}

		elseif (getutype($_SESSION['login']) == "1" && $prepaid == 1 && $credit > 0)
		{
?>
			<td colspan="2" align="center">
			<b>Vous êtes connecté et il vous reste <?=$credit?> minutes.</b>
			</td>
<?
		}

		// mode non prépayé ou mode permanent
		else
		{
?>
			<td colspan="2" align="center">
			<b>Vous êtes connecté.</b>
			</td>
<?
		}
?>
		</tr><tr>
		<td></td>
		</tr><tr>
		<td align="center" width="50%">
		  <a href="membre.php" target="blank"><img src="images/accueil.png"></a>
		</td>
		<td align="center" width="50%">
		  <a href="#" onClick="sortie()"><img src="images/fin.png"></a>
		</td>
		</tr>
		</tr><tr>
		  <td align="center"><font size="1" color="black">Accueil</font></td>
		  <td align="center"><font size="1" color="black">Déconnexion</font></td>
		</tr>
		</table>
		</body>
		</html>
<?
	}
}
?>
