<?php
require_once "includes/config.php";
require "includes/database.php";
require "includes/fonctions.php";

session_name("EVOAUTH_PHPSESSION");
session_start ();

// accès OK
if (isset($_SESSION['login']))
{
?>
	<html>
	<head>
	<link rel="StyleSheet" href="style.css" type="text/css">
	<title><?=$title;?></title>
	</head>
	<body link="white" vlink="white" alink="white">
	<table align="center" border="0">
	<tr>
	<p align="center"><?=$banner_member;?>
	</tr>
	<tr>
	<td colspan="2" align="center"><img src="images/<?=$pic;?>"</td>
	</tr>
	<tr>
	<td>&nbsp;</td>
	</tr>
<?
	if ($_SESSION['login'] != "admin")
	{
		// on récupère le crédit de l'utilisateur
		$credit = getcredit($_SESSION['login']);
		$utype = getutype($_SESSION['login']);

		if ($prepaid == 1 && $credit <= 0 && $utype = 0)
		{
?>
			<tr>
			<td align="center">
			  <br><br>
			  <u><i>Votre crédit est épuisé.</i></u>
			  <br><br>
			</td>
			</tr>
<?
		}

		else if ($prepaid == 1 && $credit > 0 && $utype = 0)
		{
?>
			<tr>
			<td align="center">
			  <br><br>
			  <i>Il vous reste <?=$credit?> minutes.</i>
			  <br><br>
			</td>
			</tr>
			<tr bgcolor="b4b4b5">
			<td align="center"><a href="activation.php"><b>Activation de votre accès</b></a><br></td>
			</tr>
<?
		}

		else
		{
?>
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr bgcolor="b4b4b5">
			<td align="center"><a href="activation.php"><b>Activation de votre accès</b></a><br></td>
			</tr>
<?
		}
?>
		<tr>
		<td>&nbsp;</td>
		</tr>
		<tr bgcolor="b4b4b5">
		<td align="center"><a href="edit.php?mode=pass"><b>Changement de votre mot de passe</b></a><br></td>
		</tr>
<?
	}

	if ($_SESSION['login'] == "admin")
	{
?>
		<tr bgcolor="b4b4b5">
		<td align="center"><a href="edit.php?mode=listing"><b>Gestion des utilisateurs</b></a><br></td>
		</tr>
		<tr>
		<td align="center">&nbsp;</td>
		</tr>
		<tr bgcolor="b4b4b5">
		<td align="center"><a href="edit.php?mode=accueil"><b>Modification du texte d'accueil</b></a><br></td>
		</tr>
<?
	}
?>
		<tr>
		<td align="center">&nbsp;</td>
		</tr>
		<tr bgcolor="b4b4b5">
		<td align="center"><a href="index.php"><b>Déconnexion</b></a><br></td>
		</tr>
		</table>
		
		<br />
<?
		// on ouvre accueil.txt
		if (!$file = fopen("accueil.txt", "r")) {
			echo "Echec de l'ouverture du texte d'accueil (accueil.txt)";
		}

		else {
			$accueil = "";

			// on parcourt accueil.txt
			while (!feof($file))
			{
				$accueil .= fgets($file, 255);
			}

			// on affiche le texte d'accueil
			echo $accueil;
		}

		fclose($file);
?>		
		</body>
		</html>
<?
}

// accès refusé
else
{
	kick("Accès refusé.");
}
?>
