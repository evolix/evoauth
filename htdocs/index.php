<?
require_once "includes/config.php";

// création de la session
session_name("EVOAUTH_PHPSESSION");
session_start();

// destruction de la session
session_unset("EVOAUTH_PHPSESSION");
session_destroy();
?>

<html>
<head>
<link rel="StyleSheet" href="style.css" type="text/css">
<title><?=$website;?></title>
</head>
<body alink="black" vlink="black">
<form method="post" action="login.php">
<table align="center" border="0" width="300">

<tr>
  <td align="center" colspan="3" class="header"><img src="images/<?=$pic;?>" /></td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td align="center" colspan="3" class="header"><b><?=$website;?></b></td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>

<?
	if ($Access == 0) {
?>

<tr>
  <td align="right" ><b><i>Login</i></b></td>
  <td align="left"><input type="text" name="login" /></td>
</tr>
<tr>
  <td align="right"><b><i>Mot de passe</i></b></td>
  <td align="left"><input type="password" name="pass" /></td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td align="left"><input type="submit" value="Connexion" /></td>
</tr>

<?
	}
?>

</table>
</form>

<p align="center">
<br><br><br><font color="grey">Evoauth version 0.8</font>
</p>
</body>
</html>
