<?php
	require_once "includes/config.php";
	require "includes/database.php";
	require "includes/fonctions.php";

	//md5 du mot de passe admin : "echo -n PASS | md5sum"
	$apass = "15e1dd7a1ab7eac39387ccfcbad90263";
	
	// administrateur ?
	if ($_POST['login'] == "admin" && md5($_POST['pass']) == $apass)
	{
		$ulog = $_POST['login'];
		$upass = $_POST['pass'];
		$ugroup = "admin";
		login($ulog, $upass, $ugroup);
	}

	// utilisateur ?
	else if (md5($_POST['pass']) == getpass($_POST['login']) && getactif($_POST['login']) == "1")
	{
		$ulog = $_POST['login'];
		$upass = getpass($_POST['login']);
		$ugroup = getgroup($_POST['login']);
		login($ulog, $upass, $ugroup);
	}

	else
	{
		kick("Accès refusé.");
	}

	// creation de la session
	function login($ulog,$upass,$ugroup)
	{
		// adresse ip de la machine utilisée
		$ip = getenv("REMOTE_ADDR");
	
		session_name("EVOAUTH_PHPSESSION");
   		session_start ();

		// on vérifie que l'utilisateur ne soit pas désactivé
		$actif  = getactif($_POST['login']);

		if ($actif != 1 && $_POST['login'] != "admin")
			header ('location: index.php');

		// établissement des variables de session
		$_SESSION['login'] = $_POST['login'];

		if ($_POST['login'] == "admin")
			$_SESSION['group'] = "adm";
		else
			$_SESSION['group'] = $ugroup;

		$_SESSION['ip'] = $ip;
		$_SESSION['mac'] = getmac($ip);

		// on ne décrémentera par la suite que le crédit des
		// utilisateurs crédités
		if ($_POST['login'] != "admin")
			$_SESSION['credit'] = getutype($_SESSION['login']);

		header ('location: membre.php');

		clean_ip($_POST['login'],$ip);
		update_ip($_POST['login'], $ip);
	}
?>
