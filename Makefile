# $Id$
#
# Makefile pour Evoauth

VERSION=	0.9
SRC=		./
WWWDIR=		/var/www/htdocs
BINDIR=		/usr/local/bin
DOCDIR=		/usr/local/share/doc
LOGDIR=		/var/log
NAME=		evoauth
TAR?=		tar
INSTALL?=	install

install:all
all:	web tools help msg

web:
	$(INSTALL) -m 0755 -d $(WWWDIR)/$(NAME)/{,includes,images}
	$(INSTALL) -m 0644 htdocs/*.php $(WWWDIR)/$(NAME)
	$(INSTALL) -m 0644 htdocs/style.css $(WWWDIR)/$(NAME)
	$(INSTALL) -m 0644 htdocs/includes/*.php $(WWWDIR)/$(NAME)/includes
	$(INSTALL) -m 0666 -b htdocs/accueil.txt $(WWWDIR)/$(NAME)
	$(INSTALL) -m 0644 htdocs/images/*.* $(WWWDIR)/$(NAME)/images
	$(INSTALL) -m 0644 htdocs/favicon.ico $(WWWDIR)/$(NAME)

tools:	module
	$(INSTALL) -m 0774 -d /etc/evoauth
	$(INSTALL) -m 0770 admin/Evoauth.pl $(BINDIR)/evoauth
	$(INSTALL) -m 0600 install/evoauth.conf /etc/evoauth
	$(INSTALL) -m 0600 install/evoauth.rules /etc/evoauth

module:
	cd admin/ && \
		perl Makefile.PL && \
		make && \
		make install && \
		make clean

help:
	$(INSTALL) -m 0755 -d $(DOCDIR)/$(NAME)
	$(INSTALL) -m 0644 AUTHORS LICENSE README TODO $(DOCDIR)/$(NAME)
	$(INSTALL) -m 0755 -d $(DOCDIR)/$(NAME)/install
	$(INSTALL) -m 0644 install/*.* $(DOCDIR)/$(NAME)/install
	$(INSTALL) -m 0644 install/INSTALL $(DOCDIR)/$(NAME)/install
	$(INSTALL) -m 0664 -b install/evoauth.log $(LOGDIR)

msg:
	@echo ""
	@echo "Félicitations. Evoauth est à présent installé."
	@echo "----------------------------------------------"
	@echo ""
	@echo "Consultez $(DOCDIR)/$(NAME)/install/INSTALL"
	@echo "pour plus d'informations sur l'installation."
	@echo ""

deinstall:
	rm -rf $(BINDIR)/$(NAME)
	rm -rf $(DOCDIR)/$(NAME)
	rm -rf $(WWWDIR)/$(NAME)

	rm -rf /usr/libdata/perl5/Evoauth/
	cat /usr/local/libdata/perl5/site_perl/i386-openbsd/auto/Evoauth/.packlist | xargs sudo rm -rf
