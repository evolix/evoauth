DROP DATABASE IF EXISTS evoauth032005;
CREATE DATABASE evoauth032005;
USE evoauth032005;

GRANT ALL PRIVILEGES ON evoauth032005.* to evoauth_user@localhost identified by '';
GRANT ALL PRIVILEGES ON evoauth032005.* to evoauth_user identified by '';

CREATE TABLE users (
id BIGINT NOT NULL AUTO_INCREMENT,
login VARCHAR(20) NOT NULL,
pass VARCHAR(255) NOT NULL,
groupe VARCHAR(20) NOT NULL,
utype INT(10) NOT NULL,
credit INT(10) NOT NULL,
ip VARCHAR(20) NOT NULL,
statut INT(1) NOT NULL,
actif INT(1) NOT NULL,
firstcon VARCHAR(30) NOT NULL,
lastupdate VARCHAR(30) NOT NULL,
kick INT(1) NOT NULL,
PRIMARY KEY (id) );

INSERT INTO users (login, pass, groupe, actif, utype) VALUES ("daphnee", "098f6bcdr4621d373cade4e832627b4f6", "general", "1", "0");
INSERT INTO users (login, pass, groupe, actif, utype, credit) VALUES ("pierre", "098f6bcdr4621d373cade4e832627b4f6", "general", "1", "1", "60");
INSERT INTO users (login, pass, groupe, actif, utype) VALUES ("paul", "098f6bcdr4621d373cade4e832627b4f6", "general", "1", "0");
